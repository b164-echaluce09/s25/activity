//display fruits per country

db.fruits.aggregate([
            { $unwind : "$origin" },
            { $group : { _id : "$origin" , fruits : { $sum : 1 } } }
        ]);

//count the total number of fruits onsale

db.fruits.aggregate([
    {$match: { onSale: true } },
    { $group: {_id: "$name", fruitsonSale: {$sum: "$name"} } },
    { $count:"fruitsonSale"}
])

// count the total number of fruits with stock more than or equal to 20.

db.fruits.aggregate([
    {$match:  { stock: { $gte: 20 } } },
    { $group: {_id: "$name", enoughStock: {$sum: "$name"} } },
    { $count:"enoughStock"}
])


//average price of fruits onSale per supplier

db.fruits.aggregate([
    {$match: { onSale: true } },
    { $group: {_id: "$supplier_id", avg_price: {$avg: "$price"} } }
    ])

//get the highest price of a fruit per supplier.

db.fruits.aggregate([
    {$match: { onSale: true } },
    { $group: {_id: "$supplier_id", max_price: {$max: "$price"} } }
    ])

//get the lowest price of a fruit per supplier.

db.fruits.aggregate([
    {$match: { onSale: true } },
    { $group: {_id: "$supplier_id", min_price: {$min: "$price"} } }
    ])
